
import Icon from './img/abcd.svg';

export default {
	id: "abcd",
	meta: {
		title: 	{
			"de": "ABCD-Quiz",
			"en": "ABCD-Quiz"
		},
		text:	{
			"de": "Interaktives Lernspiel, ein wenig wie \"Wer wird Millionär\" für viele Teilnehmer",
			"en": "Interactive educational game, similar to „Wer wird Millionär?“ for many participants"
		},
		to: 	"abcd-index",
		adminto:"abcd-admin",
		role:	"abcd",
		icon: 	Icon,
		index:	true,
	},
	routes: [
		{	path: '/abcd-index', name:'abcd-index', component: () => import('./views/Index.vue') },
		{	path: '/abcd/:key', name:'abcd-login-key', component: () => import('./views/Part.vue') },
		{	path: '/abcd/', name:'abcd-login', component: () => import('./views/Part.vue') },
		{	path: '/abcd-run', name:'abcd-run', component: () => import('./views/Run.vue') },
		{	path: '/abcd-show/:ticket', name:'abcd-show', component: () => import('./views/Show.vue') },
		{	path: '/abcd-edit', name:'abcd-edit', component: () => import('./views/Edit.vue') },
		{	path: '/abcd-admin', name:'abcd-admin', component: () => import('./views/Admin.vue') },
	],
	imageMaxSize:102400
}
